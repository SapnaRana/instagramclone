export default {
    images:{
        heartIcon: require('../../assets/heart.png'),
        commentIcon: require('../../assets/comment.png'),
        sendIcon: require('../../assets/send.png'),
    },

    styleConstants:{
        rowHeight:50
    }
}