import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import config from '../../config';



//MARK:- UserBar
const UserBarView = React.memo(() => {
    return (
        <View style={styles.userBar}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                    style={styles.userPic}
                    source={{
                        uri: "https://cdn.fastly.picmonkey.com/contentful/h6goo9gw1hh6/2sNZtFAWOdP1lmQ33VwRN3/24e953b920a9cd0ff2e1d587742a2472/1-intro-photo-final.jpg?w=1200&h=992&q=70"
                    }}
                />
                <View style={{marginLeft: 10 }}>
                <Text style={{fontSize:20,fontWeight:'600'}}>RyanAByrne</Text>
                <Text style={{fontSize:15}}>Jodhpur, Rajastan, India</Text>
                </View>

            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ fontSize: 30 }}>...</Text>
            </View>
        </View>
    )
})

//MARK:- Bottom Icon Bar
const BottomIconBar = React.memo(({ heartIconColor }) => {
    return (
        <View style={styles.iconBar}>
            <Image resizeMode='stretch' style={{ width: 30, height: 30, marginLeft: 8, tintColor: heartIconColor }} source={config.images.heartIcon} />
            <Image resizeMode='stretch' style={{ width: 30, height: 30, marginLeft: 8 }} source={config.images.commentIcon} />
            <Image resizeMode='stretch' style={{ width: 30, height: 30, marginLeft: 8 }} source={config.images.sendIcon} />
        </View>
    )
})

//MARK:- PosterView
const PosterView = React.memo(props => {

    return (
        <View style={{ height: 400, width: props.getScreenWidth }}>
            <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => { props.likedTogged() }}>
                <Image
                    style={{ width: props.getScreenWidth, height: 400 }}
                    source={{
                        uri: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12231413/Labrador-Retriever-MP.jpg"
                    }} />
            </TouchableOpacity>
        </View>
    )
}
)

class Post extends Component {

    constructor(props) {
        super(props)
        this.state = {
            liked: false,
            getScreenWidth: 0,
        }
    }

    likedTogged = () => {
        // alert('heloo')
        this.setState({
            liked: !this.state.liked
        })
    }
    componentDidMount() {
        this.setState({
            getScreenWidth: Dimensions.get('window').width
        });
    }


    render() {

        const heartIconColor = (this.state.liked) ? 'rgb(252,61,57)' : null

        return (
            <View style={{ flex: 1, width: 100 + "%", height: 100 + "%" }}>
                <UserBarView />
                <PosterView
                    likedTogged={this.likedTogged}
                    getScreenWidth={this.state.getScreenWidth} />
                <BottomIconBar
                    heartIconColor={heartIconColor} />

                    <View style={[styles.iconBar,{marginHorizontal:10}]}>
                        <Image
                         style={[styles.iconBar, {width:30,height:30,marginRight:5}]}
                         source={config.images.heartIcon}
                        />
                        <Text>128 Likes</Text>
                    </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    topNav: {
        width: "100%",
        height: 56,
        marginTop: 20,
        backgroundColor: 'rgb(250,250,250)',
        borderBottomColor: 'rgb(233,233,233)',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    userBar: {
        width: "100%",
        height: config.styleConstants.rowHeight + 10,
        backgroundColor: 'rgb(255,255,255)',
        flexDirection: 'row',
        paddingHorizontal: 10,
        justifyContent: 'space-between',
    },
    userPic: {
        width: 40,
        height: 40,
        borderRadius: 20,
    },
    iconBar: {
        height: config.styleConstants.rowHeight,
        width: "100%",
        borderColor: 'rgb(233,233,233)',
        borderTopWidth: StyleSheet.hairlineWidth,
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: 'row',
        alignItems: 'center'

    },
    commentBar:{
        width:"100%",
        backgroundColor: 'rgb(233,233,233)',
        borderTopWidth:React.hairlineWidth,
        borderBottomWidth:React.hairlineWidth
    }



})


export default Post;

