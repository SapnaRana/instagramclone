import React, { Component } from 'react';
import { Post } from '../presentation'
import { FlatList, Text, View } from 'react-native'

const Renderpost = React.memo(props => {
    return (
        <Post/>
    )
})
class PostFeed extends Component {

    _renderPost({ item }) {
        return <Post/>
    }
    _returnKey(item) {
        return item.toString();
    }
    render() {
        return (
                <FlatList
                    data={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}
                    keyExtractor={this._returnKey}
                    renderItem={this._renderPost}
                    // renderItem={({ item }) => <Renderpost user={item} />}
                />
        )
    }
}
export default PostFeed;
