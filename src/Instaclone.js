import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView,Image} from 'react-native'
import { PostFeed } from './components/container'
// import LinearGradient from 'react-native-lin'


//MARK:-  TopNavigation Bar
const TopNavigationBar = React.memo(() => {
    return (
        <View style={styles.topNav}>
            <Text style={{ fontSize: 25, fontWeight: '300' }}>
                Instagram
        </Text>
        </View>
    )
})


//MARK:-  TopNavigation Bar
const StoryUser = React.memo(() => {
    return (
        <View style={{justifyContent:'center',alignItems:'center',width:100,height: 100,marginLeft:10}}>
            <View style={{width:60,height: 60,borderRadius :30,borderColor:'red',borderWidth:2,marginBottom: 8,justifyContent:'center',alignItems:'center'}} >
            <Image
                    style={{width:50,height:50,borderRadius:25}}
                    source={{
                        uri: "https://cdn.fastly.picmonkey.com/contentful/h6goo9gw1hh6/2sNZtFAWOdP1lmQ33VwRN3/24e953b920a9cd0ff2e1d587742a2472/1-intro-photo-final.jpg?w=1200&h=992&q=70"
                    }}
                />
            
            </View>
            <Text style={{fontSize:18,fontWeight:'300'}} numberOfLines={1}>Pankaj Singh</Text>
            </View>
    )
})

const data = [
    'Goa',
    'Gujrat',
    'Madhya Pradesh',
    'Assam',
    'Gujrat',
    'Karnataka',
    'Jharkhand',
    'Himachal Pradesh',
    'Manipur',
    'Meghalaya',
    'Mizoram',
    'Uttarakhand',
    'West Bengal',
    'Tamil Nadu ',
    'Punjab',
    'Rajasthan',
    'Bihar',
    'Andhra Pradesh',
    'Arunachal Pradesh',
  ];

const StoryView = React.memo(() =>{
    return(
        <View style={{height:100,width:'100%',borderBottomWidth:1,borderBottomColor:'rgb(233,233,233)'}}>
            <ScrollView horizontal={true}>
            
               <StoryUser/>
               <StoryUser/>
           
            </ScrollView>

        </View>
    )

})



class Instaclone extends Component {
    constructor(props) {
        super(props)
      
    }

    render() {


        return (
            <SafeAreaView>
                <View style={{width:'100%',height:'100%'}}>
                <TopNavigationBar/>
                <StoryView/>
                <PostFeed/>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    topNav: {
        width: "100%",
        height: 56,
        marginTop: 20,
        backgroundColor: 'rgb(250,250,250)',
        borderBottomColor: 'rgb(233,233,233)',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})



export default Instaclone;

